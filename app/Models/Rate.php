<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rates';
    protected $fillable = [
    	'id', 'rate','blog_id','member_id'
    ];
    public $timestamps = true;
}
