<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|unique:users,email|email|max:255 ',
            'password' => 'required|string|min:8|confirmed|',
            'phone' => 'required|string|max:12',
            'img' =>   'required|image|mimes:jpeg,png,jpg,gif',
            'country' => 'required'
        ];
    }
}
