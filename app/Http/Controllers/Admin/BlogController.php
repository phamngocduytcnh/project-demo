<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddBlogRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog;
use App\User;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
        return view('admin.blog.add');
    }

    public function create(AddBlogRequest $request){
        $blog = new Blog();
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->content =  $request->content;
        $blog->userId = Auth::id();
        $blogs = Blog::all();
        if($blog->save()){
            return redirect()->to('admin/blog/list')->with(['success' => 'Add blog is successfull !!! ']);
        }
        else {
            return redirect()->to('admin/blog/list')->with(['error' => 'Add blog is error !!! ']);
        }
        
    }
    public function list(){
        $blogs = Blog::paginate(2);
        $user = Auth::user();
        return view('admin.Blog.list', compact('blogs', 'user'));
    }
    
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $blog = Blog::find($id);
        return view('admin/Blog/edit', compact('blog'));
    }

    public function edit(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->content = $request->content;
        if($blog->save() ) {
            return redirect('admin/blog/list')->with(['success' => 'Update blog is successfull !!! ']);
        }
        else {
            return redirect('admin/blog/list')->with(['error' => 'Update blog is error !!! ']);
        }
    }

    public function delete($id){
        $blog = Blog::find($id);
        if ($blog->delete()) {
            return redirect('admin/blog/list')->with(['success' => 'Delete blog is successfull !!! ']);
        }
        else {
            return redirect('admin/blog/list')->with(['error' => 'Delete blog is successfull !!! ']);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
