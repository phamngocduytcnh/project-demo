<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UpdateRequest;
use App\User;
use App\Models\Country;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Decode a JSON object to PHP object
        if(Auth::check()){
            $user = json_decode (Auth::user());
            $countrys = Country::all();
            return view('admin.user.profile', compact('user','countrys'));
        }
    }
    //
    public function updateProfile(UpdateRequest $request){
        if(Auth::check()){
            $user = json_decode(Auth::user());
            $getPassword =  $request->input('password');
            $getUser = User::find($user->id);
            $getUser->name = $request->input('name');
            $getUser->phone = $request->input('phone');
            $getUser->countryId = (int)$request->input('country');
            if(isset($getPassword)){
                $getUser->password = Hash::make($getPassword);
            }
           
            if($request->hasFile('img')){
                $file = $request->file('img');
                unlink(public_path('uploads\profiles\\'.$user->img));
                $getUser->img = $file->getClientOriginalName();
                $file->move('uploads\profiles', $file->getClientOriginalName());
            }
            $getUser->save();
            $countrys = Country::all();
            return redirect('admin/profile');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
