<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    public function show(){
    	$blogs = Blog::paginate(2);
    	return response()->json([
    		'message' => 'Success',
    		'blog' => $blogs,
    	]);
    }
}
