<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginMemberRequest;

class LoginController extends Controller
{
    public function memberLogin(LoginMemberRequest $request){
    	$data = [
    		'email' => $request->email,
    		'password' => $request->password,
    		'level' => 0
    	];
    	$remember = $request->has('remember') ? true : false;
    	if(Auth::attempt($data, $remember)){
    		return response()->json([
    			'message' => 'Login is succesfull !',
    			'user' => Auth::user()
    		]);
    	}
    	else {
    		return response()->json([
    			'message' => 'Login is failled !'
    		]);
    	}
    }
}
