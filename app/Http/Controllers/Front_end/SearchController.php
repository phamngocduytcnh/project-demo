<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\State;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Support\Facades\DB; 

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchAdvance(Request $request)
    {
        $query = DB::table('products');
        if($request->state != '0'){
            $query->where('state_id', (int)$request->state);
        }
        if($request->brand != '0'){
            $query->where('brand_id', (int)$request->brand);
        }
        if($request->category != '0'){
            $query->where('category_id', (int)$request->category);
        }
        if($request->price != '0'){
            $query->orderByRaw('price * (1 - sale_off/100)' .$request->price);
        }
        $products = $query->get();
        $array = [];
        foreach ($products as $product) {
            $data = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'sale_off' =>  $product->sale_off,
                'img' => asset('uploads/products/'.$product->member_id.'/'.json_decode($product->img)[0] ) ,
            ];
            $array[] = $data;
        }
        return response()->json(['array' => $array]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $categorys = Category::all();
        $brands = Brand::all();
        $states = State::all();
        $products = Product::all();
        return view('front-end.search.search',compact('categorys','brands','states','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
