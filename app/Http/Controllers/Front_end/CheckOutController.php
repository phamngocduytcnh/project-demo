<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Mail\CartEmail;

class CheckOutController extends Controller
{
    public function show(Request $request){
    	$total = 0;
    	$cart = $request->session()->get('cart');
        if(isset($cart) && !empty($cart) )
        {
            foreach ($cart as  $item) {
                $total += $item['price'] * $item['quanity'];
            }
        }
    	if(isset($cart) && !empty($cart)){
    		return view('front-end.checkout.checkout', compact('cart','total'));
    	}
    	return back();
    }
    public function sendMail(Request $request){
    	if(Auth::check()){
    		$cart = $cart = $request->session()->get('cart');
            $total = 0;
            foreach ($cart as  $item) {
                $total += $item['price'] * $item['quanity'];
            }
    		if(isset($cart) && !empty($cart) )
    		{
    			Mail::to(Auth::user()->email)->send(new CartEmail($cart, $total));
                $request->session()->forget('cart');
    			dd('Email is sent');
    		}

    	}
    	return back()->with(['error' => 'You must be login']);
    }
}
