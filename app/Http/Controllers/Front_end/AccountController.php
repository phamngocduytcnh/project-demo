<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileMemberRequest;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\Models\Country;
use App\Models\Category;
use App\Models\State;
use App\Models\Brand;
use App\Models\Product;
use App\User;
use Image;
use Exception;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    protected function updateAvatar($file,$deletedImg)
    {
        $target_dir = 'uploads\profiles\\';
        $path_deletedImg = public_path($target_dir . $deletedImg);
        $file->move($target_dir,strtotime(date('Y-m-d H:i:s'))."_". $file->getClientOriginalName());
        unlink($path_deletedImg);
    }

    protected function sendSuccessResponse()
    {
        return back()->with(['success'=>'Successfull !!']);
    }

    protected function sendFailedResponse()
    {
        return back()->with(['error'=>'Error !!! ']);
    }

    protected function uploadImgProduct($files, $user_id){
        $arrayImg = [];
        foreach ($files as $image) 
        {
            $name = strtotime(date('Y-m-d H:i:s'))."_".$image->getClientOriginalName();
            $name_2 = "small_".strtotime(date('Y-m-d H:i:s'))."_".$image->getClientOriginalName();
            $name_3 = "medium_".strtotime(date('Y-m-d H:i:s'))."_".$image->getClientOriginalName();
            $path = public_path('uploads/products/'. $user_id .'/'.$name);
            $path2 = public_path('uploads/products/'. $user_id .'/'.$name_2);
            $path3 = public_path('uploads/products/'. $user_id .'/'.$name_3);
            //$image->move('uploads\products', $image->getClientOriginalName());
            //Kiểm tra một folder có tồn tại hay không, nếu không thì tạo
            $dir_path = public_path('uploads/products/'. $user_id );
            if(!File::isDirectory($dir_path))
            {
                File::makeDirectory($dir_path, 0777, true, true);
            }
            Image::make($image->getRealPath())->save($path);
            Image::make($image->getRealPath())->resize(85, 84)->save($path2);
            Image::make($image->getRealPath())->resize(330, 380)->save($path3);
            
            $arrayImg[] = $name; 
        }
        return $arrayImg;    
    }

    public function deleteImageProduct($user_id, $img)
    {
        unlink(public_path('uploads\products\\'. $user_id .'\\'.$img));
        unlink(public_path('uploads\products\\'. $user_id .'\\'.'small_'.$img));
        unlink(public_path('uploads\products\\'. $user_id .'\\'.'medium_'.$img));
    }

    public function deleteImageProductSelected($arrayImg, $request, $user_id){
        foreach ($request as $check) {
            foreach ($arrayImg as $key => $img) {
                if($img == $check){
                    array_splice($arrayImg,$key,1);
                    $this->deleteImageProduct($user_id, $img );
                }
            }
        }
        return $arrayImg;
    }
    public function showProfile()
    {
        $countrys = Country::all();
        $user = Auth::user();
        return view('front-end.account.profile',compact('countrys', 'user'));
    }

    public function updateProfile(UpdateProfileMemberRequest $request)
    {
    	$user = Auth::user();
        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'countryId' => (int)$request->country,
            'message' => $request->message
        ];
        if( !$request->password == ''){
            $data['password'] = Hash::make($request->password);
        }
        if($request->hasFile('img')){
            $file = $request->file('img');
            $this->updateAvatar( $file, $user->img);
            $data['img']= strtotime(date('Y-m-d H:i:s'))."_". $file->getClientOriginalName();
        }
        try 
        {
            $getUser = User::findOrFail($user->id);
            $getUser->update($data);
            return $this->sendSuccessResponse();
        } 
        catch(Exception $e){
            return $this->sendFailedResponse();
        }
    }
    public function formAddProduct()
    {
        $categorys = Category::all();
        $brands = Brand::all();
        $states = State::all();
    	return view ('front-end.account.create-product', compact('categorys','brands','states'));
    }
    public function addProduct(AddProductRequest $request)
    {
        $data = [
            'name' => $request->name,
            'price' => $request->price,
            'member_id' => Auth::user()->id,
            'category_id' => $request->category,
            'brand_id' => $request->brand,
            'state_id' => $request->state
        ];
        $state = State::find($request->state);
        if($state->state == 'Sale'){
            $data['sale_off'] = $request->saleoff;
        }
        else {
            $data['sale_off'] = 0;
        }
        try 
        {
            if($request->hasFile('img')){
                $files = $request->file('img');
                $arrayImg = $this->uploadImgProduct($files, Auth::user()->id);
                $data['img'] = json_encode($arrayImg);
            }
            Product::create($data);
            return $this->sendSuccessResponse();
        }
        catch(Exception $e){
            return $this->sendFailedResponse();
        }
    }
    public function listProduct()
    {
        $products = Product::where('member_id', Auth::user()->id)->paginate(2);
        $user = Auth::user();
        return view('front-end.account.list-product', compact('products','user'));
    }
    public function editFormProduct($id)
    {
        $product = Product::findOrFail($id);
        $user = Auth::user();
        $categorys = Category::all();
        $brands = Brand::all();
        $states = State::all();
        return view('front-end.account.edit-product', compact('product','brands','states', 'categorys','user'));
    }
    public function editProduct(UpdateProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $arrayImg = json_decode($product->img);
        $countImg = count($arrayImg);
        $countCheck = $countRequest = 0;
        //Count số ảnh upload lên
        if($request->hasFile('img')){
            foreach ($request->file('img') as $image){
                $countRequest++;
            }
        }
        //Count số ảnh xóa ( checked)
        if($request->has('check')){
            foreach ($request->check as $check) {
                $countCheck++;
            }
        }
        //Check không vượt quá 3
        $total = $countImg + $countRequest - $countCheck;
        if($total >= 4 || $total <= 0){
            return $this->sendFailedResponse();
        }
        $data = [
            'img' => json_encode($arrayImg),
            'name' => $request->name,
            'price' => $request->price,
            'category_id' => $request->category,
            'brand_id' => $request->brand,
            'state_id' => $request->state
        ];  
        $state = State::find($request->state);
        if($state->state == 'Sale'){
            $data['sale_off'] = $request->saleoff;
        }
        else{
            $data['sale_off'] = 0;
        }
        try 
        {
            //Kiểm tra và xóa các ảnh đã chọn
            if($request->has('check')){
                $arrayImg = $this->deleteImageProductSelected($arrayImg, $request->check, Auth::user()->id);
            }
            //Add các ảnh đã chọn vào array
            if($request->hasFile('img')){
                $files = $request->file('img');
                $array_addImage = $this->uploadImgProduct($files, Auth::user()->id);
                foreach ($array_addImage as $image){
                    array_push($arrayImg, $image );
                }
            }
            $product->update($data);
            return $this->sendSuccessResponse();
        }
        catch (Exception $e)
        {
            return $this->sendFailedResponse();
        }
    }
    public function deleteProduct($id){
        try
        {
            $product = Product::findOrFail($id);
            $arrayImg = json_decode($product->img);
            if ($product->delete()) {
                //Xóa hết các ảnh trong foler: uploads/products
                foreach ($arrayImg as $key => $img) {
                    $this->deleteImageProduct(Auth::user()->id, $img);
                }
                return back()->with(['success' => 'Delete blog is successfull !!! ']);
            }
            return $this->sendSuccessResponse();
        }
        catch(Exception $e)
        {
            return $this->sendFailedResponse();
        }
    }
}
   