<?php

namespace App\Http\Controllers\Front_end;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterMemberRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Country;
use App\User;

class RegisterController extends Controller
{
    public function showRegisterForm(){
    	$countrys = Country::all();
    	return view('front-end.auth.register', compact('countrys'));	
    }
    public function sendSuccessResponse(){
        return redirect('/member/register')->with(['success' => 'Register member is successfull !!! ']);
    }
    public function sendFailledResponse(){
        return redirect('/member/register')->with(['error' => 'Register member is error !!! ']);
    }
    public function register(RegisterMemberRequest $request){
        if($request->hasFile('img')){
            $file = $request->img;
            if($file->move('uploads\profiles', $file->getClientOriginalName())){
                $img = $file->getClientOriginalName();
            }
        }
        $user = User::create([
            'name' =>  $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'level' => 0,
            'countryId' => $request->country,
            'phone' => $request->phone,
            'img' => $img,
        ]);
    	if($user){
    		return $this->sendSuccessResponse();
    	}
    	else {
    		return $this->sendFailledResponse();
    	}
    	
    }
}
