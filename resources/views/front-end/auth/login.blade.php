@extends('front-end.layouts.main')
@section('content')
	<section id="form"><!--form-->
		<div class="container">
			<div class="col-sm-4 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Login to your account</h2>
					<form method="post">
						@csrf
						<input type="text" name="email" placeholder="Email" />
						@error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                         @enderror
						<input type="password" name="password" placeholder="Password" />
						@error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
						<span>
							<input type="checkbox" class="checkbox" name="remember"> 
							Keep me signed in
						</span> 
						<button type="submit" class="btn btn-default">Login</button>
						<a href="{{url('password/reset')}}" class="btn btn-primary">Forgot your password ?</a>
					</form> <br>
					@if(session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div><!--/login form-->
			</div>
			<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
			<div class="col-sm-4">
				<div class="login-form">
					<h2>Login with Social account</h2>
					<a href="{{ url('member/login/google') }}" style="margin-top: 20px;" class="btn btn-danger btn-block">
	                   <strong>Login with Google </strong> <i class="fa fa-google-plus"></i>
	                </a>
	                <a href="{{ url('member/login/facebook') }}" style="margin-top: 20px;" class="btn btn-info btn-block">
	                   <strong>Login with Facebook </strong> <i class="fa fa-facebook"></i>
	                </a>	
				</div>
			</div>
		</div>
	</section><!--/form-->
@endsection