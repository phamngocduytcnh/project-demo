@extends('front-end.layouts.main')
@section('content')
	<section id="form"><!--form-->
		<div class="container">
			<div class="col-sm-6">
				<div class="login-form"><!--sign up form-->
					<h2>New User Signup!</h2>
					<form class="form-horizontal" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-md-12">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line" value="{{ old('name') }}" id="name" name="name" >
                            </div>
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                         @enderror
                        <div class="form-group">
                            <label for="email" class="col-md-12">Email</label>
                            <div class="col-md-12">
                                <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="email" id="email" >
                            </div>
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label class="col-md-12">Password</label>
                            <div class="col-md-12">
                                <input type="password" class="form-control form-control-line" id="password" name="password">
                            </div>                           
                        </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label class="col-md-12">Confirm Password:</label>
                            <div class="col-md-12">
                                <input type="password" class="form-control form-control-line" id="password_confirmation" name="password_confirmation" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-md-12">Click to Upload Avatar here : </label>
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="img" id="uploadImg" >
                            </div>
                            <div class="col-md-12">
                                <img src="" id="showImg">
                            </div>
                        </div>
                        @error('img')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label class="col-md-12">Phone No</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" id="phone" name="phone" >
                            </div>                           
                        </div>
                        @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label class="col-md-12">Message</label>
                            <div class="col-md-12">
                                <textarea rows="5" class="form-control form-control-line"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Country</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="country" name="country">
                                    @foreach($countrys as $country)
                                    	<option value="{{$country->id}}">{{$country->country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Register</button>
                            </div>
                        </div>
                    </form>
                    @if(session('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{{session('success')}}</li>
                            </ul>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{session('error')}}</li>
                            </ul>
                        </div>
                    @endif
				</div><!--/sign up form-->
			</div>
		</div>
	</section><!--/form-->
    <script type="text/javascript">
        $(document).ready(function(){
        
        // Show Images before Upload Image
            function readURLImage(input){
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    reader.onload = function(e) {
                      $('#showImg').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#uploadImg").change(function(){
                readURLImage(this);
            })
        })
    </script>
@endsection
