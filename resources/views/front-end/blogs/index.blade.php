@extends('front-end.layouts.main')
@section('content')
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						@foreach($blogs as $blog)
							<div class="single-blog-post">
								<h3>{{$blog->title}}</h3>
								<div class="post-meta">
									<ul>
										<li><i class="fa fa-user"></i>{{$blog->User['name']}}</li>
										<li><i class="fa fa-clock-o"></i>{{ date_format( $blog->created_at, 'h-i-sa')  }}</li>
										<li><i class="fa fa-calendar"></i> {{ date_format( $blog->created_at, 'M-d-Y')  }}</li>
									</ul>
									<span>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-half-o"></i>
									</span>
								</div>
								<p>{{$blog->description}}</p>

								{!! $blog->content !!}

								<a  class="btn btn-primary" href="{{url('/blog/list/'.$blog->id )}}">Read More</a>
							</div>
						@endforeach
						<div class="pagination-area">
							{{ $blogs->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection