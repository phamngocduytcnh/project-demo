@extends('front-end.layouts.main')
@section('content')
			<div class="col-sm-8 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Update your account</h2>
					<form class="form-horizontal" method="post" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="name" class="col-md-12">Full Name</label>
							<div class="col-md-12">
								<input type="text" id="name" name="name" value="{{$user->name}}" >
							</div>
						</div>
						@error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
							<label for="email" class="col-md-12">E-mail:</label>
							<div class="col-md-12">
								<input type="email" id="email" name="email" value="{{$user->email}}" readonly />
							</div>
						</div>
						@error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                         @enderror
                        <div class="form-group">
							<label for="password" id="password" class="col-md-12">New Password :</label>
							<div class="col-md-12">
								<input type="password" id="password" name="password" />
							</div>
						</div>
						@error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label  class="col-md-12">Confirm New Password:</label>
                            <div class="col-md-12">
                                <input type="password" class="form-control form-control-line" id="password_confirmation" name="password_confirmation" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="uploadImg" class="col-md-12">Avatar Upload: </label>
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="img" id="uploadImg">
                            </div>
                            <div class="col-md-12">
                                <img src="{{asset('uploads/profiles/'.$user->img.'')}}" id="showImg">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-md-12">Phone No</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control form-control-line" id="phone" name="phone" value="{{$user->phone}}" >
                            </div>                           
                        </div>
                        @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
						<div class="form-group">
                            <label class="col-md-12">Message</label>
                            <div class="col-md-12">
                                <textarea rows="5" class="form-control form-control-line" name="message">{{$user->message}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Country</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="country" name="country" >
                                    @foreach($countrys as $country)
                                    	<option value="{{$country->id}}">{{$country->country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>  
						<button type="submit" class="btn btn-default">Update</button>
					</form><br>
					@if(session('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{{session('success')}}</li>
                            </ul>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{session('error')}}</li>
                            </ul>
                        </div>
                    @endif
				</div><!--/login form-->
			</div>
		</div>
	</section><!--/form--><br>
	<script type="text/javascript">
        $(document).ready(function(){
        
        // Show Images before Upload Image
            function readURLImage(input){
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    reader.onload = function(e) {
                      $('#showImg').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#uploadImg").change(function(){
                readURLImage(this);
            })
        })
    </script>
@endsection