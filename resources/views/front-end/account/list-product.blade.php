@extends('front-end.layouts.main')
@section('content')
<style type="text/css">
	td img {
		width: 75px;
		height: 100px;
		margin-right: 5px;
	}
	#fullImg{
		width: 100%;
		height: auto;
	}
</style>
			<div class="col-sm-8 col-sm-offset-1">
				<div class="table-responsive">
					@if(session('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{{session('success')}}</li>
                            </ul>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{session('error')}}</li>
                            </ul>
                        </div>
                    @endif
					<table class="table">
						<thead>
		        			<tr>
		        				<th scope="col">Id</th>
		        				<th scope="col">Name</th>
		        				<th scope="col">Price</th>
		        				<th scope="col">Image</th>
		                        <th scope="col">Action</th>
		                        <th scope="col">Action</th>
		                        <th scope="col">Action</th>
		        			</tr>
		        		</thead>
		        		<tbody>
		        			@foreach($products as $product)
		        				<tr>
		        					<td>{{$product->id}}</td>
		        					<td>{{$product->name}}</td>
		        					<td>{{$product->price.' $'}}</td>
		        					<td>
		        						@foreach(json_decode($product->img) as $key => $img)
		        							@if($key == 0)
		        								<a href="" rel="prettyPhoto"><img src="{{asset('uploads/products/'.$user->id.'/'.$img.'')}}"></a>
		        							@endif
		        						@endforeach
		        					</td>
		        					<td>
		        						<a href="{{url('member/account/product/list/'.$product->id.'/edit')}}" class="btn btn-info">Edit</a>
		        					</td>
		        					<td>
		        						<a href="{{url('member/account/product/list/'.$product->id.'/delete')}}" class="btn btn-danger">Delete</a>
		        					</td>
		        					<td>
		        						<a href="{{url('product/list/'.$product->id.'')}}" class="btn btn-success">Blog detail</a>
		        					</td>
		        				</tr>
		        			@endforeach
		        			<tr>
		                        <td></td>
		                        <td></td>
		                        <td></td>
		                        <td></td>
		                        <td></td>
		                        <td></td>
		                        <td><a href="{{route('member.createProduct')}}" class="btn btn-success" >Add new</a></td>
		                    </tr>
		        		</tbody>
		        	</table>
		        	{{ $products->links() }}
				</div>
			</div>
		</div>
	</section><!--/form--><br>
	<script type="text/javascript">
		$(document).ready(function(){
			$("a > img").click(function(){
				var src = $(this).attr('src');
				$(this).closest('a').attr('href', src);
			});
			$("a[rel^='prettyPhoto']").prettyPhoto();
		})
	</script>
@endsection