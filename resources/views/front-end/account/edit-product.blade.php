@extends('front-end.layouts.main')
@section('content')
<style type="text/css">
	#showImg img {
		width: 75px;
		height: 100px;
		margin-right: 5px;
	}
	.selectImg {
		display: inline-block;
		margin-right: 5px;
	}
</style>
			<div class="col-sm-8 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Update product</h2>
					<form class="form-horizontal" method="post" enctype="multipart/form-data">
						@csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{session('success')}}
                            </div>
                        @endif
						<div class="form-group">
							<label for="name" class="col-md-12">Product name: </label>
							<div class="col-md-12">
								<input type="text" id="name" name="name" value="{{$product->name}}">
							</div>
						</div>
                        <div class="form-group">
							<label for="name" class="col-md-12">Price: </label>
							<div class="col-md-12">
								<input type="number" id="price" name="price" min="0" value="{{$product->price}}">
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Category :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="category" name="category">
                                    @foreach($categorys as $category)
                                    	@if($category->id == $product->id)
                                    		<option value="{{$category->id}}" selected="">{{$category->category}}</option>
                                    	@else
                                    		<option value="{{$category->id}}">{{$category->category}}</option>
                                    	@endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Brand :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="brand" name="brand">
                                	@foreach($brands as $brand)
                                   		@if($brand->id == $product->id)
                                        	<option value="{{$brand->id}}" selected>{{$brand->brand}}</option>
                                        @else
                                        	<option value="{{$brand->id}}">{{$brand->brand}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Status :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="state" name="state">
                                	@foreach($states as $state)
                                		@if($state->id == $product->state_id)
                                        	<option value="{{$state->id}}" selected>{{$state->state}}</option>
                                        @else
                                        	<option value="{{$state->id}}">{{$state->state}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="saleoff">
                            <label class="col-sm-12">Sale off :</label>
                            <div class="col-sm-12">
                            	<input type="number" name="saleoff" value="{{$product->sale_off}}" min="0">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="uploadImg" class="col-md-12">Product Image: </label>
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="img[]" id="uploadImg" multiple>
                            </div>
                            <div class="col-md-12" id="showImg">
                            	@foreach(json_decode($product->img) as $key => $img)
		        					<div class="selectImg">
		        						<img src="{{asset('uploads/products/'.$user->id.'/'.$img.'')}}">
		        						<input type="checkbox" value="{{$img}}" name="check[]">
		        					</div>
		        				@endforeach
                            </div>
                            <div id="errorImg"></div>
                        </div>             
                        <div class="form-group">
                            <label class="col-md-12">Message</label>
                            <div class="col-md-12">
                                <textarea rows="5" class="form-control form-control-line" name="message"></textarea>
                            </div>
                        </div>
						<button type="submit" class="btn btn-default" id="update-product">Update product</button>
					</form><br>
				</div><!--/login form-->
			</div>
		</div>
	</section><!--/form--><br>
	<script type="text/javascript">
		$(document).ready(function(){
			if($("#state option:selected").text() == 'Sale'){
				$('#saleoff').show();
			}
			else{
				$('#saleoff').hide();
			}
			$('#state').change(function(){
				var selected =$(this).children("option:selected").text();
				if(selected == 'Sale'){
					$('#saleoff').show();
				}
				else{
					$('#saleoff').hide();
				}
			})
            var total = countCheck = countUpload = 0;
            var countImage = parseInt("{{count(json_decode($product->img))}}");
			//Đếm số lượng Image được chọn để xóa
			$("input[type='checkbox']").click(function(){
				if($(this).is(":checked")){
					countCheck++; 
				}
                else{
                    countCheck--;
                }
			})
			function readURLImage(input, placeShowImg){
				if(input.files){
					var count = input.files.length;
					for(var i = 0; i < count; i++){
						var reader = new FileReader();
						reader.onload = function(event) {
                    		$($.parseHTML("<img class='add'>")).attr('src', event.target.result).appendTo(placeShowImg);
                		};
                		reader.readAsDataURL(input.files[i]);
					}
				}
			}
            function imgUpload(input){
                if(input.files){
                    var count = input.files.length;
                    return count;
                }
                return 0;
            }
			//
			$('#uploadImg').on('change', function(){
				$('.add').remove();
				readURLImage(this,'div#showImg');
                countUpload = imgUpload(this);
			})
            $(document).on('submit','form', function(){
                var total = countImage + countUpload - countCheck;
                if(total <= 0){
                    $('#errorImg').html("<span class='alert alert-danger'> Must be least an image </span>");
                    total = 0;
                    return false;
                }
                else if(total >= 4){
                    $('#errorImg').html("<span class='alert alert-danger'>You can only upload 3 image </span>");
                    total = 0;
                    return false;
                }
                else{
                    return true;
                }
            })
		})
	</script>
@endsection

