@extends('front-end.layouts.main')
@section('content')
<style type="text/css">
	#showImg img{
		display: inline-block;
		width: 150px;
		height: 200px;
		margin-right: 10px;
	}
</style>
			<div class="col-sm-8 col-sm-offset-1">
				<div class="login-form"><!--login form-->
					<h2>Create new product</h2>
					<form class="form-horizontal" method="post" enctype="multipart/form-data">
						@csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{{session('success')}}</li>
                            </ul>
                        </div>
                        @endif
                        @if(session('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
                            </div>
                        @endif
						<div class="form-group">
							<label for="name" class="col-md-12">Product name: </label>
							<div class="col-md-12">
								<input type="text" id="name" name="name">
							</div>
						</div>
                        <div class="form-group">
							<label for="name" class="col-md-12">Price: </label>
							<div class="col-md-12">
								<input type="number" id="price" name="price" min="0">
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Category :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="category" name="category">
                                    @foreach($categorys as $category)
                                        <option value="{{$category->id}}">{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Brand :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="brand" name="brand">
                                   @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->brand}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12">Select Status :</label>
                            <div class="col-sm-12">
                                <select class="form-control form-control-line" id="state" name="state">
                                	@foreach($states as $state)
                                        <option value="{{$state->id}}">{{$state->state}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="saleoff">
                            <label class="col-sm-12">Sale off :</label>
                            <div class="col-sm-12">
                            	<input type="number" name="saleoff" value="0" min="0">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="uploadImg" class="col-md-12">Product Image: </label>
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="img[]" id="uploadImg" multiple>
                            </div>
                            <div class="col-md-12" id="showImg"></div>
                            <div class="" id="errorImg"></div>
                        </div>             
                        <div class="form-group">
                            <label class="col-md-12">Message</label>
                            <div class="col-md-12">
                                <textarea rows="5" class="form-control form-control-line" name="message"></textarea>
                            </div>
                        </div>
						<button type="submit" class="btn btn-default" id="add-product">Add product</button>
					</form><br>
				</div><!--/login form-->
			</div>
		</div>
	</section><!--/form--><br>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#saleoff').hide();
			$('#state').change(function(){
				var selected =$(this).children("option:selected").text();
				if(selected == 'Sale'){
					$('#saleoff').show();
				}
				else{
					$('#saleoff').hide();
				}
			})
            var count = 0;
			function readURLImage(input, placeShowImg){
				if(input.files){
					var count = input.files.length;
					for(var i=0; i< count; i++){
						var reader = new FileReader();
						reader.onload = function(event) {
                    		$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeShowImg);
                		};
                		reader.readAsDataURL(input.files[i]);
					}
				}
			}
			function imgUpload(input){
                if(input.files){
                    var count = input.files.length;
                    return count;
                }
                return 0;
            }

			$('#uploadImg').on('change', function(){
                $('div#showImg').html('');
				readURLImage(this,'div#showImg');
                count = imgUpload(this);
			})
            $(document).on('submit','form', function(){
                if(count > 3)
                {
                    $('#errorImg').html("<span class='alert alert-danger'> You are only allowed to select 3 image <span>");
                    count = 0;
                    return false;
                }
                else 
                {
                    return true;
                }
            })

		})
	</script>
@endsection