@include('front-end.layouts.header')
@if( Request::is('member/login') || Request::is('member/register') || Request::is('cart') || Request::is('checkout'))

@elseif(Request::is('member/account/*'))
	@include('front-end.layouts.menu-left-account')
@else
	@include('front-end.layouts.menu-left')
@endif
@yield('content')
@include('front-end.layouts.footer')