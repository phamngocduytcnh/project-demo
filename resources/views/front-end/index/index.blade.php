@extends('front-end.layouts.main')
@section('content')
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<div id="show">
						@foreach($products as $product)
							<div class="col-sm-3">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{asset('uploads/products/'.$product->member_id.'/medium_'.json_decode($product->img)[0].'')}}" alt="" />
											@if($product->State['state'] == 'New')
												<h2>${{$product->price}}</h2>
											@else
												<h2>${{$product->price * (1 - $product->sale_off/100)}}<del style="margin-left: 20px;">${{$product->price}}</del></h2>
											@endif
											<p>{{$product->name}}</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												@if($product->State['state'] == 'New')
													<h2>${{$product->price}}</h2>
												@else
													<h2>${{$product->price * (1 - $product->sale_off/100)}}</h2>
												@endif
												<p>{{$product->name}}</p>
												<a href="#" id="{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
										</div>
										@if($product->State['state'] == 'New')
											<img src="{{asset('front-end/images/home/new.png')}}" class="new" alt="" />
										@else
											<img src="{{asset('front-end/images/home/sale.png')}}" class="sale" alt="" />
										@endif
									</div>
									<div class="choose">
										<ul class="nav nav-pills nav-justified">
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
										</ul>
									</div>
								</div>
							</div>
						@endforeach
						</div>
					</div><!--features_items-->
					
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>
								<li><a href="#blazers" data-toggle="tab">Blazers</a></li>
								<li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>
								<li><a href="#kids" data-toggle="tab">Kids</a></li>
								<li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="tshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery1.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery2.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery3.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery4.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="blazers" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery4.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery3.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery2.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery1.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="sunglass" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery3.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery4.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery1.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery2.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="kids" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery1.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery2.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery3.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery4.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
							<div class="tab-pane fade" id="poloshirt" >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery2.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery4.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery3.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('front-end/images/home/gallery1.jpg')}}" alt="" />
												<h2>$56</h2>
												<p>Easy Polo Black Edition</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div><!--/category-tab-->
					
					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend1.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend2.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend3.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend1.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend2.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="{{asset('front-end/images/home/recommend3.jpg')}}" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div><!--/recommended_items-->
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $(document).on('click','a.add-to-cart', function(e){
		    	e.preventDefault();
		    	var productId = $(this).attr('id');
		    	$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxAddProduct')}}",
		    		data : {
		    			'productId' : productId
		    		},
		    		success:function(data){
		    			alert(data.message);
		    		}
		    	});

		    })
		    $(document).on('click','.slider-track', function(){
		    	var value = $(".tooltip-inner").text();
		    	var position_space = value.indexOf(' ');
		    	var min = parseInt(value.slice( 0, position_space) );
		    	position_space = value.lastIndexOf(' ');
		    	var max = parseInt(value.slice( position_space) );
		    	$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxPriceRange')}}",
		    		data : {
		    			'min' : min,
		    			'max' : max
		    		},
		    		success:function(data){
		    			$("#show").empty();
		    			var html = "";
		    			for (i in data.array){
		    				html += "<div class='col-sm-3'>"
		    							+ "<div class='product-image-wrapper'>"
		    								+ "<div class='single-products'>"
		    									+ "<div class='productinfo text-center'>"
		    										+ "<img src='" + data.array[i].img + "'> ";
		    										if(data.array[i].sale_off == 0)
		    										{
		    											html +=  "<h2>$" + data.array[i].price + "</h2>";
		    										}
		    										else 
		    										{
		    											html += "<h2>$"+ data.array[i].price * (1-data.array[i].sale_off/100) +"<del style='margin-left: 20px;''>$"+ data.array[i].price +"</del></h2>";
		    										}
		    										html += "<p>" + data.array[i].name + "</p>"
		    										+ "<a class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a> "
		    									+ "</div>"
		    									+ "<div class='product-overlay'>"
		    										+ "<div class='overlay-content'>";
		    											if(data.array[i].sale_off == 0)
		    											{
		    												html +=  "<h2>$" + data.array[i].price + "</h2>";
		    											}
		    											else 
		    											{
		    												html += "<h2>$"+ data.array[i].price * (1-data.array[i].sale_off/100) + "</h2>";
		    											}
		    											html += "<p>" + data.array[i].name + "</p>"
		    											+ "<a id='"+data.array[i].id + "' class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"
		    										+ "</div>"
		    									+ "</div>";
		    									if(data.array[i].sale_off == 0)
	    										{
	    											html += "<img src='{{asset('front-end/images/home/new.png')}}'' class='new' alt='' />";
	    										}
	    										else 
	    										{
	    											html += "<img src='{{asset('front-end/images/home/sale.png')}}'' class='sale' alt='' />";
	    										}
	    										html +=
		    								 "</div>"
		    								+ "<div class='choose'>"
													+ "<ul class='nav nav-pills nav-justified'>"
														+ "<li><a ><i class='fa fa-plus-square'></i>Add to wishlist</a></li>"
														+ "<li><a ><i class='fa fa-plus-square'></i>Add to compare</a></li>"
													+ "</ul>"
											+ "</div>"
		    							+ "</div>"
		    						+ "</div>";		    				
		    			}
		    			$("#show").html(html);
		    		}
		    	});
		    })
		    //
		    $("input#search-product").keyup(function(){
		    	var search = $(this).val();
		    	$.ajax({
		    		type: 'POST',
		    		url : "{{url('ajaxSearchIndex')}}",
		    		data : {
		    			'search' : search
		    		},
		    		success:function(data){
		    			$("#show").empty();
		    			var html = "";
		    			for (i in data.array){
		    				html += "<div class='col-sm-3'>"
		    							+ "<div class='product-image-wrapper'>"
		    								+ "<div class='single-products'>"
		    									+ "<div class='productinfo text-center'>"
		    										+ "<img src='" + data.array[i].img + "'> ";
		    										if(data.array[i].sale_off == 0)
		    										{
		    											html +=  "<h2>$" + data.array[i].price + "</h2>";
		    										}
		    										else 
		    										{
		    											html += "<h2>$"+ data.array[i].price * (1-data.array[i].sale_off/100) +"<del style='margin-left: 20px;''>$"+ data.array[i].price +"</del></h2>";
		    										}
		    										html += "<p>" + data.array[i].name + "</p>"
		    										+ "<a class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a> "
		    									+ "</div>"
		    									+ "<div class='product-overlay'>"
		    										+ "<div class='overlay-content'>";
		    											if(data.array[i].sale_off == 0)
		    											{
		    												html +=  "<h2>$" + data.array[i].price + "</h2>";
		    											}
		    											else 
		    											{
		    												html += "<h2>$"+ data.array[i].price * (1-data.array[i].sale_off/100) + "</h2>";
		    											}
		    											html += "<p>" + data.array[i].name + "</p>"
		    											+ "<a id='"+data.array[i].id + "' class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"
		    										+ "</div>"
		    									+ "</div>";
		    									if(data.array[i].sale_off == 0)
	    										{
	    											html += "<img src='{{asset('front-end/images/home/new.png')}}'' class='new' alt='' />";
	    										}
	    										else 
	    										{
	    											html += "<img src='{{asset('front-end/images/home/sale.png')}}'' class='sale' alt='' />";
	    										}
	    										html +=
		    								 "</div>"
		    								+ "<div class='choose'>"
													+ "<ul class='nav nav-pills nav-justified'>"
														+ "<li><a ><i class='fa fa-plus-square'></i>Add to wishlist</a></li>"
														+ "<li><a ><i class='fa fa-plus-square'></i>Add to compare</a></li>"
													+ "</ul>"
											+ "</div>"
		    							+ "</div>"
		    						+ "</div>";		    				
		    			}
		    			$("#show").html(html);
		    			search = '';
		    		}
		    	});

		    })
		});
	</script>
@endsection